import numpy as np
import scipy as sp
from scipy.signal import chirp
from scipy import constants

def generate_afg_chirp(
        chirp_rep_time = 0.001,
        turn_freq = 470151.0308,
        chirp_middle = 317250,
        chirp_dev = 11.75e3,
        sampling_freq = 1e6):
    chirp_length_time = chirp_rep_time
    
    # Generate One Chirp
    chirp_time_samples = np.arange(0, chirp_length_time, 1/sampling_freq)
    chirp_signal_samples = sp.signal.chirp(chirp_time_samples, chirp_middle-chirp_dev, chirp_length_time, chirp_middle+chirp_dev)

    chirp_lasts_for_turns = chirp_length_time * turn_freq

    return chirp_time_samples, chirp_signal_samples, chirp_lasts_for_turns


def generate_chirp(
        chirp_turns=256,
        chirp_rep_time=0.001,
        turn_freq=470151.0308,
        chirp_start_freq=2e4,
        chirp_stop_freq=8e4,
        sampling_freq=1e6):
    chirp_length_time = chirp_turns / turn_freq
    # Generate one chirp
    chirp_time_samples = np.arange(0, chirp_length_time, 1/sampling_freq)
    chirp_signal_samples = sp.chirp(chirp_time_samples, chirp_start_freq, chirp_length_time, chirp_stop_freq)

    # Generate one repetition
    rep_time_samples = np.arange(0, chirp_rep_time, 1/sampling_freq)[:-1] # cut off the end for the next repetition
    rep_signal_samples = np.pad(chirp_signal_samples, 
                                (0, int(chirp_rep_time * sampling_freq - chirp_length_time * sampling_freq)), 
                                'constant', 
                                constant_values=(0, 0))
    
    return rep_time_samples, rep_signal_samples

def generate_chirp_signal(
        chirp_turns=256,
        chirp_repetitions=500,
        chirp_rep_time=0.001,
        turn_freq=470151.0308,
        chirp_start_freq=2e4,
        chirp_stop_freq=8e4,
        sampling_freq=1e6):
    """
    Generate a chirp signal for the simulation.
    ---
    Returns:
    time_samples: np.ndarray [s]
        Time samples of the signal
    signal_samples: np.ndarray [1]
        Signal samples
    total_turns: float [1]
        Total number of turns in the signal
    ---
    Parameters:
    chirp_turns: int [1]
        Number of turns that one chirp lasts for. Default: 256
    chirp_repetitions: [1]
        Number of chirp repetitions. Default: 500
    chirp_rep_time: float [s]
        Time between chirp repetitions. Default: 0.001 (1ms)
    turn_freq: float [Hz]
        Frequency of one turn in the machine. Default: 470151.0308
    chirp_start_freq: float [Hz]
        Start frequency of the chirp. Default: 2e4
    chirp_stop_freq: float [Hz]
        Stop frequency of the chirp. Default: 8e4
    sampling_freq: float [Hz]
        Sampling frequency of the signal. Default: 1e6
    """
    total_signal_time = chirp_repetitions * chirp_rep_time
    chirp_length_time = chirp_turns / turn_freq
    # Generate one chirp
    chirp_time_samples = np.arange(0, chirp_length_time, 1/sampling_freq)
    chirp_signal_samples = sp.signal.chirp(chirp_time_samples, chirp_start_freq, chirp_length_time, chirp_stop_freq)

    # Generate one repetition
    rep_time_samples = np.arange(0, chirp_rep_time, 1/sampling_freq)[:-1] # cut off the end for the next repetition
    rep_signal_samples = np.pad(chirp_signal_samples, 
                                (0, int(chirp_rep_time * sampling_freq - chirp_length_time * sampling_freq)), 
                                'constant', 
                                constant_values=(0, 0))
    
    # Generate full signal
    time_samples = np.arange(0, total_signal_time, 1/sampling_freq)[:-1] # cut off the end for the next repetition
    signal_samples = np.pad(
        rep_signal_samples,
        (0, len(rep_signal_samples)*(chirp_repetitions-1)),
        'wrap'
    )

    # calculate the number of turns
    total_time = chirp_rep_time * chirp_repetitions
    total_turns = total_time * turn_freq
    return time_samples, signal_samples, total_turns


# TFB Config
def kick_angle(gain, Brho):
    c = constants.c # m/s, speed of light
    E0 = 0.93827e9 # eV, proton rest energy
    mu0 = 4*np.pi*(10**-7) # H/m, vacuum permeability

    T = (np.sqrt(Brho**2+E0/1e9**2)-E0/1e9)*1e9 # eV, proton KE
    P = 5e3 # W, TFB peak power / electrode
    Z = 100 # Ohm, TFB impedance / electrode

    L = 935e-3 # m, TFB length
    r = 70e-3 # m, TFB separation

    E = T + E0 # Total Energy
    gamma = E / E0 # Normalized energy (lorentz)
    beta = np.sqrt(1-gamma**-2) # normalised velocity (lorentz)
    cp = np.sqrt(E**2 - E0**2) # eV, particle momentum

    # Electric Field
    Vp = np.sqrt(P * Z * 2) # peak voltage
    V = Vp * gain
    Efield = V / r # adjusted for gain

    # Magnetic Field
    I = np.sqrt((V**2/Z)/Z*2) # current
    Hfield = (2*I) / (2*np.pi*r) # adjusted for gain
    Bfield = Hfield * mu0

    # Angle
    theta_M = c/1e9*Bfield*L / (cp/10**9)
    theta_E = Efield/1e9*L / (cp/10**9 * beta)
    theta = theta_E + theta_M
    return theta
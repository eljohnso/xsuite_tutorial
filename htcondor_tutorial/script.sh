#!/bin/bash
# This is a shebang. It specifies to the system that this script should be run using bash.

python3 -m venv venv
# This command creates a new Python virtual environment in a directory called "venv".

source venv/bin/activate
# This command activates the virtual environment. Once activated, any Python commands
# (like pip or python) will use this environment's version of Python and its installed packages.

pip install -r requirements.txt
# This command installs Python packages from the requirements.txt file into the activated virtual environment.
# The requirements.txt file is a list of Python package names (and optionally version numbers), one per line.

python3 script.py "$@"
# This command runs the Python script named script.py, passing it any arguments that were given to this bash script.
# "$@" is a special variable in bash that represents all of the arguments given to the script.

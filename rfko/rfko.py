# Import simulation packages
import xtrack as xt
import xpart as xp
import xobjects as xo
from cpymad.madx import Madx

# Import scientific packages
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import numpy as np
import pandas as pd
import scipy as sp
from scipy import constants

# Import custom signal generator
from signal_gen import generate_afg_chirp

# Import timing packages
from tqdm import tqdm
import time as timing

# Import standard packages
import os
import json
import sys
import pickle as pkl
from collections import Counter

# Conversion factors
RAD_TO_MRAD = 1000
M_TO_MM = 1000
MS_TO_S = 1e-3

# Beam parameters
REL_GAMMA = 25.598474067
REL_BETA = np.sqrt(1-REL_GAMMA**(-2))

exn = 2e-6
eyn = 2e-6
ex = exn/(REL_BETA*REL_GAMMA)
ey = eyn/(REL_BETA*REL_GAMMA)

p = 5.392 # beam momentum (GeV/c)
momentum = p # beam momentum (GeV/c)
Brho = p*3.3356

DPP_FACTOR = 1.54e-3

PARENT_DIR = "."
os.makedirs(PARENT_DIR, exist_ok=True)

# Use the PS lattice as of 24 may
LATTICE = "ps_24May.seq"

print(f"Parent Directory: {PARENT_DIR}")
mad = Madx(stdout=False)

# Import PS lattice
mad.call(LATTICE)
mad.command.beam(
    particle="PROTON",
    pc = p,
    ex = ex,
    ey = ey,
    charge = 1
)
mad.input(f"BRHO      := BEAM->PC * 3.3356;")
mad.use(sequence="PS")

# turn off 23 for ions
mad.input("kPEBSW23 := 0;")
mad.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K1L,KEYWORD,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APERTYPE,APER_1,APER_2,APER_3,APER_4,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')
mad.input('savebeta, label=bumped23, place = PR.BPM23;')

def tune_match(Qx,Qxp,Qy,Qyp,p,ex,ey):
    
    mad.input('''
    ptc_twiss_macro(order, dp, slice_flag): macro = {
    ptc_create_universe;
    ptc_create_layout, time=false, model=2, exact=true, method=6, nst=3;
    IF (slice_flag == 1){
        select, flag=ptc_twiss, clear;
        select, flag=ptc_twiss, column=name,keyword,s,l,x,px,beta11,beta22,disp1,k1l;
        ptc_twiss, closed_orbit, icase=56, no=order, deltap=dp, table=ptc_twiss, summary_table=ptc_twiss_summary, slice_magnets=true;
    }
    ELSE{
        select, flag=ptc_twiss, clear;
        select, flag=ptc_twiss, column=name,keyword,s,x,px,beta11,alfa11,beta22,alfa22,disp1,disp2,mu1,mu2,energy,l,angle,K1L,K2L,K3L,HKICK,SLOT_ID;    
        ptc_twiss, closed_orbit, icase=56, no=order, deltap=dp, table=ptc_twiss, summary_table=ptc_twiss_summary, normal;
    }
    ptc_end;
    };

    ''')
    
    #/**********************************************************************************
    # *                        Matching using the PFW
    #***********************************************************************************/
    mad.input("Qx   := "+str(Qx)+"; !Horizontal Tune")
    mad.input("Qxp  := "+str(Qxp)+"; !Horizontal Chromaticity")

    mad.input("Qy   := "+str(Qy)+"; !Vertical Tune")
    mad.input("Qyp  := "+str(Qyp)+"; !Vertical Chromaticity")

    mad.input('''
    use, sequence=PS;
    match, use_macro;
            vary, name = k1prpfwf;
            vary, name = k1prpfwd;
            vary, name = k2prpfwf;
            vary, name = k2prpfwd;
            use_macro, name = ptc_twiss_macro(2,0,0);
            constraint, expr = table(ptc_twiss_summary,Q1)  = Qx;
            constraint, expr = table(ptc_twiss_summary,Q2)  = Qy;
            constraint, expr = table(ptc_twiss_summary,DQ1) = Qxp;
            constraint, expr = table(ptc_twiss_summary,DQ2) = Qyp;
    jacobian,calls=50000,bisec=3;
    ENDMATCH;
    ''')
    
    mad.use(sequence="PS")
    twiss_tune_matching = mad.twiss().dframe() # Needed to refresh the tune values
    mad.input('qx = table(SUMM, Q1);')
    mad.input('qy = table(SUMM, Q2);')
    mad.input('qxp = table(SUMM, DQ1);')
    mad.input('qyp = table(SUMM, DQ2);')
    
    tune_info_dict = {"Qx": mad.globals["qx"], "Qy": mad.globals["qy"], "Qxp": mad.globals["qxp"], "Qyp": mad.globals["qyp"]}
    pfw_dict = {"k1prpfwf": mad.globals["k1prpfwf"], "k1prpfwd": mad.globals["k1prpfwd"], "k2prpfwf": mad.globals["k2prpfwf"], "k2prpfwd": mad.globals["k2prpfwd"]}

    print (f"H-tune: {round(tune_info_dict['Qx'],3)}, H-Chroma: {round(tune_info_dict['Qxp'],3)}\nV-Tune: {round(tune_info_dict['Qy'],3)}, V-Chroma: {round(tune_info_dict['Qyp'],3)}")
    print (f"")
    print (f"PFW settings: \n  k1prpfwf: {round(pfw_dict['k1prpfwf'],6)}\n  k1prpfwd: {round(pfw_dict['k1prpfwd'],6)}\n  k2prpfwf: {round(pfw_dict['k2prpfwf'],6)}\n  k2prpfwd: {round(pfw_dict['k2prpfwd'],6)}")
    
    return pfw_dict, tune_info_dict

# Data taken from Qmeter
QX_TARGET = 0.32
QPX_TARGET = -0.5

QY_TARGET = 0.256
QPY_TARGET = -0.242

# Match tune using PFWs
pfw_dict_on_resonance, tune_info_on_resonance = tune_match(Qx=QX_TARGET,Qxp=QPX_TARGET,Qy=QY_TARGET,Qyp=QPY_TARGET, p=p, ex=ex, ey=ey)

mad.input("k1prpfwf = "+str(pfw_dict_on_resonance["k1prpfwf"])+";")
mad.input("k1prpfwd = "+str(pfw_dict_on_resonance["k1prpfwd"])+";")
mad.input("k2prpfwf = "+str(pfw_dict_on_resonance["k2prpfwf"])+";")
mad.input("k2prpfwd = "+str(pfw_dict_on_resonance["k2prpfwd"])+";")

# Makethin with 4 slices
QUAD_SLICE = 4
mad.use(sequence='ps')
mad.select(flag='makethin', class_='rbend', slice_=QUAD_SLICE)
mad.select(flag='makethin', class_='quadrupole', slice_=QUAD_SLICE)
mad.select(flag='makethin', class_='sbend', slice_=QUAD_SLICE)
mad.select(flag='makethin', class_='hkicker', slice_=QUAD_SLICE, thick=False)
mad.select(flag='makethin', class_='sextupole', slice_=2)
mad.makethin(sequence='ps')
twiss_after_makethin = mad.twiss().dframe()

print("> Import Complete, MAD-X is ready to go!")

def run_rfko(
    volt = 1,               # tfb voltage
    interval = 1,           # chirp repetition interval milliseconds
    rf10MHz_enable = '0',   # 10MHz cavities on/off
    time = 0.4,             # total sim time seconds
    n_part = int(2e3),      # particles generated
    n_turn = None,          # total sim time override
    context = "GPU"
):
    print("> run_rfko()")

    # Use CLA if available
    print(sys.argv[1:])
    try:
        args = sys.argv[1:]
        volt = args[0]
        interval = args[1]
        rf10MHz_enable = args[2]
        time = args[3]
        n_part = args[4]
        n_turn = args[5]
        context = args[6]
    except:
        print("> No CLA, defaulting")
        pass
    else:
        print(f"> CLA found")
        print(f"> Interval: {interval}")
        print(f"> Voltage: {volt}")
        print(f"> 10MHz RF: {rf10MHz_enable}")
        print(f"> time: {time}")
        print(f"> n_part: {n_part}")
        print(f"> n_turn: {n_turn}")
        print(f"> context: {context}")

    # Context Override
    if context == "GPU":
        ctx = xo.ContextCupy()
    elif context == "CPU":
        ctx = xo.ContextCpu()
    else:
        raise ValueError("Invalid context, must be GPU or CPU")
    
    # Convert Line to Xsuite
    mad.use(sequence="PS")
    line = xt.Line.from_madx_sequence(
        mad.sequence()
        )

    line.particle_ref = xp.Particles(
        mass0=xp.PROTON_MASS_EV,
        q0=1,
        p0c=p*sp.constants.c
    )

    # Calculate Frev
    line.build_tracker(_context=ctx)
    twiss_parameters = line.twiss(method='4d')
    trev = twiss_parameters['T_rev0']
    frev = 1/trev
    line.unfreeze()

    # N_turn override
    if n_turn is None or n_turn == 0 or n_turn == '0': 
        print("> n_turn not specified, calculating from time")
        n_turn = int(float(time)*frev)
    else:
        n_turn = int(n_turn)
        print("> n_turn overridden")

    rf10MHz_enable = True if rf10MHz_enable == '1' else False    

    # Septa Config
    SEPTA_X_MM = -60
    septum = xt.LimitRect(
            min_x = SEPTA_X_MM*0.001, # in meters
        )
    line.insert_element(
        element = septum,
        name = "SEPTUM",
        index = 'pe.smh57'
    )

    # TFB Config
    def kick_angle(gain):
        c = constants.c # m/s, speed of light
        E0 = 0.93827e9 # eV, proton rest energy
        mu0 = 4*np.pi*(10**-7) # H/m, vacuum permeability

        T = (np.sqrt(Brho**2+E0/1e9**2)-E0/1e9)*1e9 # eV, proton KE
        P = 5e3 # W, TFB peak power / electrode
        Z = 100 # Ohm, TFB impedance / electrode

        L = 935e-3 # m, TFB length
        r = 70e-3 # m, TFB separation

        E = T + E0 # Total Energy
        gamma = E / E0 # Normalized energy (lorentz)
        beta = np.sqrt(1-gamma**-2) # normalised velocity (lorentz)
        cp = np.sqrt(E**2 - E0**2) # eV, particle momentum

        # Electric Field
        Vp = np.sqrt(P * Z * 2) # peak voltage
        V = Vp * gain
        Efield = V / r # adjusted for gain

        # Magnetic Field
        I = np.sqrt((V**2/Z)/Z*2) # current
        Hfield = (2*I) / (2*np.pi*r) # adjusted for gain
        Bfield = Hfield * mu0

        # Angle
        theta_M = c/1e9*Bfield*L / (cp/10**9)
        theta_E = Efield/1e9*L / (cp/10**9 * beta)
        theta = theta_E + theta_M
        return theta

    rfko_kick = kick_angle(float(volt))

    # Generate a single chirp; this will be repeated by Exciter
    sampling_freq = 1e9
    chirp_time, chirp_signal, chirp_turns = generate_afg_chirp(
        chirp_rep_time = float(interval)*MS_TO_S,
        turn_freq = frev,
        chirp_middle = frev * 0.325,
        chirp_dev = frev * 0.025,
        sampling_freq = sampling_freq
    )

    # Exciter config; the duration parameter tells it to repeat the chirp_signal
    rfko_exciter = xt.Exciter(
        _context = ctx,
        samples = chirp_signal,
        sampling_frequency = sampling_freq,
        frev = frev,
        duration = float(time),
        start_turn = 0,
        knl = [rfko_kick]
    )
    line.insert_element(
        element = rfko_exciter,
        name = f'EXCITER',
        index = 'pr.kfb97'
    )

    # RF Cavity Config; voltage 0 if disabled
    if rf10MHz_enable:
        RF_VOLTAGE = 130e3 # V
    else:
        RF_VOLTAGE = 0

    RF_HARMONIC = 8
    RF_FREQUENCY = frev * RF_HARMONIC

    RF_VOLTAGE = 130e3 # V
    PHASE = 0 # rising-edge

    rf_cavity = xt.Cavity(
        _context = ctx,
        voltage = RF_VOLTAGE,
        frequency = RF_FREQUENCY,
        lag = PHASE
    )

    line.insert_element(
        element = rf_cavity,
        name = "rf_cavity",
        at_s = 0
    )

    # # Insert a monitor to record the particles
    # monitor = xt.ParticlesMonitor(
    #     num_particles = n_part,
    #     start_at_turn = 0,
    #     stop_at_turn = n_turn,
    # )
    # line.unfreeze()
    # line.insert_element(index='pe.smh57', element=monitor, name='septa_monitor')

    line.build_tracker(_context=ctx)
    line.particle_ref = xp.Particles(
        mass0=xp.PROTON_MASS_EV,
        q0=1,
        p0c=p*sp.constants.c
    )

    # Particle Generation

    if rf10MHz_enable:
        particles = xp.generate_matched_gaussian_bunch(
            line = line,
            num_particles = int(n_part),
            nemitt_x = exn,
            nemitt_y = eyn,
            sigma_z = 4.3,
            total_intensity_particles = 30e10
        )
    else:
        sigmas, sigmas_p = xp.generate_2D_gaussian(int(n_part))
        try:
            particles = line.build_particles(
                method='4d',
                delta = DPP_FACTOR,
                nemitt_x = exn,
                nemitt_y = eyn,
                x_norm=sigmas, 
                px_norm=sigmas_p,
                y_norm=sigmas,
                py_norm=sigmas_p,
            )
        except AssertionError:
            particles = xp.build_particles(
                particle_ref = line.particle_ref,
                method='4d',
                delta = DPP_FACTOR,
                nemitt_x = exn,
                nemitt_y = eyn,
                x_norm=sigmas, 
                px_norm=sigmas_p,
                y_norm=sigmas,
                py_norm=sigmas_p,
            )
    
    # Tracking
    time_before = timing.time()
    print("> Tracking...")
    for _ in tqdm(range(n_turn)):
        line.track(particles, num_turns = 1)
    time_after = timing.time()
    time_taken = time_after - time_before
    print(f"> Tracking done, {time_taken} seconds elapsed")

    particles_lost_at_each_turn = np.bincount(ctx.nparray_from_context_array(particles.at_turn))

    constants_dict = {
        "gain": volt,
        "interval": interval,
        "n_turn": n_turn,
        "frev": frev,
        "trev": trev,
        "n_part": n_part,
        "rfko_kick": rfko_kick,
        "time_taken": time_taken,
        "rf10MHz_enable": rf10MHz_enable,
        "rf10MHz_voltage": RF_VOLTAGE,
        "rf10MHz_phase": PHASE,
        "rf10MHz_frequency": RF_FREQUENCY,
        "rel_beta": REL_BETA

    }

    with open(f"{volt}_{interval}_results.pkl", "wb") as fpkl:
        pkl.dump(
            {
                "bincount": particles_lost_at_each_turn,
                "constants": constants_dict,
                "at_turn": ctx.nparray_from_context_array(particles.at_turn),
                "zeta": ctx.nparray_from_context_array(particles.zeta),
               "particles": particles.to_pandas(),
            #    "monitor": monitor.to_pandas()
            }, fpkl
        )
    print("> Dumped Results")
    print("! Script Complete !")
    return time_taken

    # [TODO) Write Benchmark wrapper
    # [TODO) Write benchmark submission, shell script